const precos = ['10', '22', '55', '9', '17'];

// const precosEmReais = precos.map(function(preco){
//     return 'R$ ' + preco + ',00';
// })

// Com arrow function
const precosEmReais = precos.map(preco => `Tá por esse valor R$ ${preco},00`);

console.log(precosEmReais);

const acimadeVinte = precos.filter(preco => preco > 20);
console.log(acimadeVinte);

