//boolean
let estaPago: boolean = true;

// number
let idade: number = 20;
let preco: number = 25.60;

//string
let nome: string = "Daniel";
let sobrenome: string = 'Lopes';

//template strings
console.log(`Meu nome é ${nome} ${sobrenome} e tenho ${idade} anos.`)

//arrays
let notas: number[] = [8.5, 7, 6, 2];

//tuple
let alunos: [string,number,string] = ['João', 10, 'Geografia'];

//enum
enum Cor {Verde, Amarelo, Azul, Vermelho};
let corFundo: Cor = Cor.Verde;

//any 
let algumValor: any = 4;
algumValor = 'Agora é uma string';
algumValor = true;

//void
function alerta(): void {
    //... alert('Operação não permitida');
}

//null e undefined

