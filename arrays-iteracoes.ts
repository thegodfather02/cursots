let notasAlunos: number[] = [10, 9, 5, 4, 9.7];

//indices - in
for(let i in notasAlunos){
    console.log(i);
}

//valores - of
for ( let notaAluno of notasAlunos){
    console.log(notaAluno)
}