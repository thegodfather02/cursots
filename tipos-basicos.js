//boolean
var estaPago = true;
// number
var idade = 20;
var preco = 25.60;
//string
var nome = "Daniel";
var sobrenome = 'Lopes';
//template strings
console.log("Meu nome \u00E9 " + nome + " " + sobrenome + " e tenho " + idade + " anos.");
//arrays
var notas = [8.5, 7, 6, 2];
//tuple
var alunos = ['João', 10, 'Geografia'];
//enum
var Cor;
(function (Cor) {
    Cor[Cor["Verde"] = 0] = "Verde";
    Cor[Cor["Amarelo"] = 1] = "Amarelo";
    Cor[Cor["Azul"] = 2] = "Azul";
    Cor[Cor["Vermelho"] = 3] = "Vermelho";
})(Cor || (Cor = {}));
;
var corFundo = Cor.Verde;
//any 
var algumValor = 4;
algumValor = 'Agora é uma string';
algumValor = true;
//void
function alerta() {
    //... alert('Operação não permitida');
}
//null e undefined
