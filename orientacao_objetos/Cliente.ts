class Cliente {

    name: string;
    age: number;

    constructor(nome: string, idade: number){
        this.name = nome;
        this.age = idade;
    }

    apresentar(){
        return `Olá meu nome é ${this.name} e tenho ${this.age} anos`;
    }

}

let joao: Cliente = new Cliente('Ronald', 21);
let showme: string = joao.apresentar();

console.log(showme);