interface Pessoa {
    idade: number;
    sexo?: string; //atributo não obrigatório
}

function imprimirIdade(pessoa: Pessoa){
    console.log(pessoa.idade);
}

// function imprimirIdade(pessoa: {idade: number}){
//     console.log(pessoa.idade);
    
// }

let pessoa2 = {nome: 'João Marcos', idade: 22};

imprimirIdade(pessoa2);