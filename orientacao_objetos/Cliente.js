var Cliente = /** @class */ (function () {
    function Cliente(nome, idade) {
        this.name = nome;
        this.age = idade;
    }
    Cliente.prototype.apresentar = function () {
        return "Ol\u00E1 meu nome \u00E9 " + this.name + " e tenho " + this.age + " anos";
    };
    return Cliente;
}());
var joao = new Cliente('Ronald', 21);
var showme = joao.apresentar();
console.log(showme);
