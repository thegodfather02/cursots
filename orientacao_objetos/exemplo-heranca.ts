class Funcionario{
    nome: String;
    salario: number;

    constructor(nome: string, salario: number){
        this.nome = nome;
        this.salario = salario;
    }

    pagarImposto(taxa: number = 7.5){
        console.log(`${this.nome} está pagando ${this.salario * taxa / 100} de imposto`);
    }

}

class Secretario extends Funcionario {

}

class Executivo extends Funcionario{
    pagarImposto(taxa: number = 27.5){
        console.log('Executivo paga bem mais');
        super.pagarImposto(taxa);        
    }
}


let pessoa1 = new Secretario('Sarah', 1500);
pessoa1.pagarImposto();

let lord = new Executivo('Jorge', 1500);
lord.pagarImposto();